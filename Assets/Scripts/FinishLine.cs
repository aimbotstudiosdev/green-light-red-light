﻿using UnityEngine;
using System.Collections;

public class FinishLine : MonoBehaviour
{
    public static FinishLine instance;
    // Start is called before the first frame update
    void Start()
    {

    }

    public void OnTriggerExit(Collider other)
    {
        //print(other.gameObject.tag);
        if (other.gameObject.CompareTag("Player") && UIManager.instance.timeLeft>0)
        {
            print("debug running");
            PlayerMovement.instance.reached = true;
            UIManager.instance.StartCoroutine("Win");
        }
        else if (other.gameObject.CompareTag("Enemy") && UIManager.instance.timeLeft > 0)
        {
            other.gameObject.GetComponent<EnemyMovement>().Celebrate();
            print("debug running for enemy");
            //UIManager.instance.StartCoroutine("Lose");
        }
    }
}
