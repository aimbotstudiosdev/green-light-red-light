using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour
{
    public GameObject blood;
    public Rigidbody rb;
    public Animator anim;
    public GameObject destination;
    public NavMeshAgent agent;
    public bool dest, reached, rangeCal, isRunning, fell;
    public float distance, range, minRange, maxRange;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CalculateRange());
    }
    public IEnumerator CalculateRange()
    {
        while (true)
        {
            if (rangeCal)
            {
                range = Random.Range(minRange, maxRange);
                rangeCal = false;
            }
            print("running");
            if (range > 30)
            {
                dest = false;
            }
            else
            {
                dest = true;
            }
            yield return new WaitForSeconds(5f);
            rangeCal = true;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (!DollMovement.instance.rotateHead && !fell)
        {
            Run();
            isRunning = true;
        }
        else if (!dest && DollMovement.instance.rotateHead)
        {
            Stop();
            isRunning = false;
        }
        if (DollMovement.instance.rotateHead && isRunning && !reached)
        {
            Fall();
        }
        if (!reached && UIManager.instance.timeLeft <= 0)
        {
            Fall();
        }
        distance = Vector3.Distance(transform.position, destination.transform.position);
        if (distance < 0.7f)
        {
            reached = true;
            anim.SetBool("Run", false);
        }

    }

    public void Run()
    {
        agent.isStopped = false;
        agent.SetDestination(destination.transform.position);
        anim.SetBool("Run", true);
    }

    public void Stop()
    {
        agent.isStopped = true;
        anim.SetBool("Run", false);
    }
    public void Fall()
    {
        fell = true;
        blood.SetActive(true);
        anim.SetBool("Fall", true);
        agent.isStopped = true;
    }
    public void Celebrate()
    {
        anim.SetBool("Celebrate", true);
    }
}
