﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelHandler : MonoBehaviour
{
    public static LevelHandler instance;
    public GameObject plane;
    public Material[] mat;

    public void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void EnvironmentChanger(int num)
    {
        plane.GetComponent<Renderer>().material = mat[num];
    }

}
