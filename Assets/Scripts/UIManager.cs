﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Threading.Tasks;
public class UIManager : MonoBehaviour
{
    public EnemyMovement enemyMov;
    public static UIManager instance;
    public GameObject panel;
    public GameObject nextLevelBtn;
    public GameObject restartBtn;
    public Text statusText, timeText;
    public int levelNum;
    public float timeLeft, minutes, seconds;
    public bool stopTimer;
    // Start is called before the first frame update
    void Start()
    {
        //Time.timeScale = 1;
    }
    
    public void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public void Update()
    {
        if (timeLeft > 0 && !stopTimer)
        {
            stopTimer = true;
            StartCoroutine(Timer());
        }

        if (timeLeft <= 0 && PlayerMovement.instance.reached == false)
        {
            PlayerMovement.instance.Fall();
            StartCoroutine(Lose());
        }
    }
    public IEnumerator Timer()
    {
        timeLeft--;
        minutes = Mathf.Floor(timeLeft / 60);
        seconds = Mathf.Floor(timeLeft % 60);

        timeText.text = $"{minutes.ToString("00")}:{seconds.ToString("00")}";
        yield return new WaitForSeconds(1f);
        stopTimer = false;
    }

    public IEnumerator Win()
    {
        PlayerMovement.instance.Celebrate();
        yield return new WaitForSeconds(5f);
        panel.SetActive(true);
        nextLevelBtn.SetActive(true);
        statusText.text = "You Win";

        Time.timeScale = 0;
    }

    public IEnumerator Lose()
    {
        yield return new WaitForSeconds(5f);
        panel.SetActive(true);
        restartBtn.SetActive(true);
        statusText.text = "You have been Eliminated";

        Time.timeScale = 0;
    }
    public void OnClickNext()
    {
        levelNum += levelNum;
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        //LevelHandler.instance.EnvironmentChanger(levelNum);
    }
    public void OnClickRestart()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
