﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MainMenuManager : MonoBehaviour
{
    /*public GameObject playPanel;
    public GameObject loadingScreen;*/
    // Start is called before the first frame update
    void Start()
    {

    }
    public void LoadLevel(int sceneIndex)
    {
        //LoadAsyncc();
        StartCoroutine(LoadAsyncc(sceneIndex));
    }
    IEnumerator LoadAsyncc(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        while (!operation.isDone)
        {
            yield return null;
        }
    }
    /*void LoadAsyncc()
    {
        loadingScreen.SetActive(true);
    }*/

}
