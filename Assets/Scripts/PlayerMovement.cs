﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public static PlayerMovement instance;
    public GameObject blood;
    public Animator anim;
    public Rigidbody rb;
    public float speed;
    public bool isRunning, fell, reached;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }
    public void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    // Update is called once per frame
    public void Update()
    {
        if (isRunning && DollMovement.instance.sameRotation && !reached)
        {
            Fall();
        }
    }
    public void FixedUpdate()
    {
        if (Input.GetMouseButton(0) && !fell && !reached)
        {
            Run();
        }
        else if(!fell)
        {
            Stop();
        }
    }
    public void Run()
    {
        rb.AddForce(0, 0, speed, ForceMode.Force);
        anim.SetBool("Run", true);
        isRunning = true;
    }
    public void Stop()
    {
        rb.velocity = new Vector3(0, 0, 0);
        anim.SetBool("Run", false);
        isRunning = false;
    }
    public void Fall()
    {
        anim.SetBool("Fall", true);
        blood.SetActive(true);
        fell = true;
        //UIManager.instance.StartCoroutine("Lose");
    }
    public void Celebrate()
    {
        anim.SetBool("Celebrate", true);
    }
}
