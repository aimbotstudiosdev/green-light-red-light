using System.Collections;
using UnityEngine;

public class DollMovement : MonoBehaviour
{
    public static DollMovement instance;
    public float lerpTime;
    public int randomTime, minRange, maxRange;
    public bool rotateHead, sameRotation;
    public Transform initRotation;
    public Transform newPos, tempPos;
    //public Quaternion initRot;

    public void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        //initRotation.rotation = transform.rotation;
        StartCoroutine(RotateHead());
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.rotation.z > 150)
        {
            print("sanjdsa");
        }
        if (transform.rotation == newPos.rotation || transform.rotation.z> tempPos.rotation.z)
        {
            sameRotation = true;
        }
        else if (transform.rotation != newPos.rotation)
        {
            sameRotation = false;
        }

        if (rotateHead)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, newPos.rotation, lerpTime * Time.deltaTime);
        }
        else
        {
            //print("rotated back");
            transform.rotation = Quaternion.Lerp(transform.rotation, initRotation.rotation, lerpTime * Time.deltaTime);
        }
    }
    public IEnumerator RotateHead()
    {
        while (true)
        {
            randomTime = Random.Range(minRange, maxRange);
            yield return new WaitForSeconds(randomTime);
            rotateHead = !rotateHead;
        }
    }
}
